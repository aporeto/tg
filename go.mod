module go.aporeto.io/tg

go 1.12

require (
	github.com/pelletier/go-toml/v2 v2.0.2 // indirect
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/afero v1.9.2 // indirect
	github.com/spf13/cobra v1.5.0
	github.com/spf13/viper v1.12.0
	github.com/subosito/gotenv v1.4.0 // indirect
	golang.org/x/sys v0.0.0-20220728004956-3c1f35247d10 // indirect
	golang.org/x/term v0.0.0-20220722155259-a9ba230a4035
	gopkg.in/ini.v1 v1.66.6 // indirect
)
